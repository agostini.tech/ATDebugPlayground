//
//  ViewController.swift
//  ATDebugPlayground
//
//  Created by Dejan on 14/01/2019.
//  Copyright © 2019 agostini.tech. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    // Simple expression assignment
    @IBAction func simpleAction() {
        testSimpleExpression()
    }
    
    private enum MyType {
        case first, second, third
    }
    
    private var type: MyType = .second
    private func testSimpleExpression() {
        switch type {
        case .first:
            print("first")
        case .second:
            print("second")
        case .third:
            print("third")
        }
    }
    
    // Code injection with a breakpoint
    @IBAction func codeInjection() {
        inject1()
        print("should call inject 2...")
    }

    private func inject1() {
        print("inject 1 was called")
    }

    private func inject2() {
        print("inject 2 was called")
    }
    
    // Conditional breakpoint
    @IBAction func incrementAction() {
        incrementCounter()
        somethingToWatch += 1
    }

    var counter = 0
    private func incrementCounter() {
        counter += 1
        print("counter value is: \(counter)")
    }
    
    // Ignore n-times
    @IBAction func iterateAction() {
        iterate()
    }
    
    private func iterate() {
        for i in 0..<100 {
            print("iteration: \(i)")
        }
    }
    
    // Instruction pointer
    @IBAction func jumpAction() {
        someFunction(param: 99)
    }

    private func someFunction(param: Int) {
        print("calling some function with param: \(param)")
    }
    
    // Watchpoints
    var somethingToWatch = 0
    @IBAction func changeVarAction() {
        somethingToWatch += 1
    }
}
